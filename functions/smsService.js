const functions = require('firebase-functions');
const axios = require('axios');

let querystring = require('querystring');

const sendSMS = functions.https.onCall(async (data, context) => {
    const { phone, text } = data
    let res = await sendToPhoneLK(phone, text)
    if (res === true) {
        return { success: true }
    } else {
        return { status: 'error', code: 402, message: "error sending sms" }
    }
});


/***
    Message must not be longer than 160 characters
    unless the maxsplit parameter is used. Must be URL encoded.
    
    Only Aus numbers
    The receiving mobile number(s). The numbers can be in the format:
        04xxxxxxxx (Australian format)
        614xxxxxxxx (International format without a preceding +)  <<<< we're doing this
        4xxxxxxxx (missing leading 0)
**/

const sendToPhoneLK = async (phoneNumber, message) => {
    try {
        let regex = new RegExp("^947[0-9]{8}?$");
        if (!regex.test(phoneNumber)) {
            console.log('invalid phone number', phoneNumber)
            return false
        }

        let postData = {
            username: 'kopark',
            password: 'U4V9fc3',
            src: 'KoPark',
            dst: phoneNumber,
            msg: message,
            dr: 1
        }

        let formData = querystring.stringify(postData);

        let options = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            url: 'http://sms.textware.lk:5000/sms/send_sms.php',
            method: 'post',
            data: formData
        }
    
        let res = await axios(options)

        if (res.status === 200) {
            console.log("SMS sent succussfully: ", res.data)
            return true
        }
        else {
            console.log("SMS Error", res.data)
            return false
        }
    } catch (err) {
        console.log(err)
        console.log("Error")
        return false
    }
}

// sendToPhoneLK('94719035291', 'test message 7')

module.exports = {
    sendSMS,
    sendToPhoneLK
}