const functions = require("firebase-functions");
const moment = require("moment");

const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

const smsService = require('./smsService');

// Listen for new vehicle
exports.onCheckInVehicle = functions.firestore.document('vehicles/{vehicleId}').onCreate(async (snap, context) => {
    const vehicleId = context.params.vehicleId
    const vehicle = snap.data();
    admin.database().ref('vehicles/' + vehicleId).set(vehicle);
    return true
});

exports.onCheckoutVehicle = functions.firestore.document('vehicles/{vehicleId}').onUpdate(async (change, context) => {
    const vehicleId = context.params.vehicleId
    // Get an object with the current document value. 
    const newDocument = change.after.data()

    // Get an object with the previous document value  
    const oldDocument = change.before.data();

    //check for vehicle checkOut
    if(oldDocument.checkedIn === true && newDocument.checkedIn === false){
        // Store data in realTime DB
        admin.database().ref('vehicles').child(vehicleId).update(newDocument)
        if(newDocument.sendReceipt === true && newDocument.phoneNumber !== ''){

            let now = moment().format('DD/MM/YYYY HH:mm:ss');
            let checkInTime = moment(new Date(newDocument.checkInTime)).format('DD/MM/YYYY HH:mm:ss');  
            let diffHours = moment.utc(moment(now,"DD/MM/YYYY HH:mm:ss").diff(moment(checkInTime,"DD/MM/YYYY HH:mm:ss"))).format("HH")
            let diffMins = moment.utc(moment(now,"DD/MM/YYYY HH:mm:ss").diff(moment(checkInTime,"DD/MM/YYYY HH:mm:ss"))).format("mm")

            let messageContent = `Dear customer, parking time duration for your vehicle is ${diffHours} hours and ${diffMins} minutes. Your fee is Rs.${diffHours * 20 + (diffMins/3).toFixed(2)}`
            let res = await smsService.sendToPhoneLK(newDocument.phoneNumber, messageContent)
            if (res === true) {
                console.log('SMS succesfully sent')
            } else {
                console.log('SMS sending failed')
            }
        }
    }
    return true
});

exports.sendSMS = smsService.sendSMS